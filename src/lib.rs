mod utils;

use calamine::{DataType, Reader, Xlsx, XlsxError};
use pyo3::create_exception;
use pyo3::exceptions::*;
use pyo3::prelude::*;
use pyo3::wrap_pyfunction;
use pyo3_file::PyFileLikeObject;
use std::io::BufReader;

use utils::CellValue;

create_exception!(price_xlsx, XlsxException, PyException);

fn _get_sheet_data(
    buffer: BufReader<PyFileLikeObject>,
    sheet: usize,
) -> Result<Vec<Vec<CellValue>>, XlsxError> {
    let mut excel: Xlsx<_> = Reader::new(buffer)?;
    let range = excel
        .worksheet_range_at(sheet)
        .expect("Нет ни одного листа")?;
    let mut result: Vec<Vec<CellValue>> = Vec::new();
    for row in range.rows() {
        let mut result_row: Vec<CellValue> = Vec::new();
        for value in row.iter() {
            match value {
                DataType::Int(v) => result_row.push(CellValue::Int(*v)),
                DataType::Float(v) => result_row.push(CellValue::Float(*v)),
                DataType::String(v) => result_row.push(CellValue::String(String::from(v))),
                DataType::DateTime(v) => result_row.push(CellValue::DateTime(*v)),
                DataType::Bool(v) => result_row.push(CellValue::Bool(*v)),
                DataType::Error(_) => result_row.push(CellValue::None),
                DataType::Empty => result_row.push(CellValue::None),
            };
        }
        result.push(result_row);
    }
    Ok(result)
}

#[pyfunction]
#[pyo3(text_signature = "buffer: BytesIO, sheet: int")]
fn get_sheet_data(buffer: PyObject, sheet: usize) -> PyResult<Vec<Vec<CellValue>>> {
    let py_file = PyFileLikeObject::with_requirements(buffer, true, false, false)?;
    let buf_reader = BufReader::new(py_file);
    match _get_sheet_data(buf_reader, sheet) {
        Ok(r) => Ok(r),
        Err(e) => match e {
            XlsxError::Io(err) => Err(PyIOError::new_err(err.to_string())),
            _ => Err(XlsxException::new_err(e.to_string())),
        },
    }
}

fn _get_sheet_names(buffer: BufReader<PyFileLikeObject>) -> Result<Vec<String>, XlsxError> {
    let excel: Xlsx<_> = Reader::new(buffer)?;
    Ok(excel.sheet_names().to_vec())
}

#[pyfunction]
#[pyo3(text_signature = "buffer: BytesIO")]
fn get_sheet_names(buffer: PyObject) -> PyResult<Vec<String>> {
    let py_file = PyFileLikeObject::with_requirements(buffer, true, false, false)?;
    let buf_reader = BufReader::new(py_file);
    match _get_sheet_names(buf_reader) {
        Ok(r) => Ok(r),
        Err(e) => match e {
            XlsxError::Io(err) => Err(PyIOError::new_err(err.to_string())),
            _ => Err(XlsxException::new_err(e.to_string())),
        },
    }
}

#[pymodule]
fn ubibi_rust_lib(py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(get_sheet_data, m)?)?;
    m.add_function(wrap_pyfunction!(get_sheet_names, m)?)?;
    m.add("XlsxException", py.get_type::<XlsxException>())?;
    Ok(())
}
