from pandas.io.excel import ExcelFile

from .reader import CalamineExcelReader


ExcelFile._engines = {
    "calamine": CalamineExcelReader,
    **ExcelFile._engines,
}
