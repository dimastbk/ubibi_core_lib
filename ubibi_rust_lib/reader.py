from __future__ import annotations

from io import BytesIO
from typing import Any, Type

from pandas.io.excel._base import BaseExcelReader

from .ubibi_rust_lib import get_sheet_data, get_sheet_names


class CalamineExcelReader(BaseExcelReader):
    book: BytesIO

    @property
    def _workbook_class(self) -> Type:
        class __empty__:
            pass

        return __empty__

    def load_workbook(self, filepath_or_buffer: BytesIO) -> BytesIO:
        io = BytesIO(filepath_or_buffer.read())
        get_sheet_names(io)
        return io

    @property
    def sheet_names(self) -> list[str]:
        return get_sheet_names(self.book)

    def get_sheet_by_name(self, name: str) -> int:
        self.raise_if_bad_sheet_by_name(name)
        return self.sheet_names.index(name)

    def get_sheet_by_index(self, index: int) -> int:
        self.raise_if_bad_sheet_by_index(index)
        return index

    def get_sheet_data(self, sheet: int, convert_float: bool) -> list[list[Any]]:
        return get_sheet_data(self.book, sheet)
