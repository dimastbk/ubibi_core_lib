1. Устанавливаем [rust](https://rustup.rs/)
2. Заходим в виртуальное окружение ubibi_core
3. `pip install maturin`
4. Из папки ubibi_rust_lib запускаем `maturin develop --release` для сборки и установки пакета в виртуальное окружение